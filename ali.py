# -*- coding: utf-8 -*-
"""
Created on 2017/3/23

@author: zyk
"""

"""
思路：
"""


def compute():
    """
    思路： 对每个字符串s，如果s翻转的值大于s，那么就翻转
    :return:
    """
    if not input_str:
        print 0
        return
    data = input_str.split(' ')
    res = []
    for x in data:
        if x[::-1] > x:
            res.append(x[::-1])
        else:
            res.append(x)
    print res
    res.sort(cmp=strcmp)
    print res
    print int("".join(res))


def strcmp(s1, s2):
    l1, l2 = len(s1), len(s2)
    if l1 < l2:
        l = l1
    else:
        l = l2
    if s1[:l] != s2[:l]:
        return -1 if s1 > s2 else 1
    else:
        return l1 - l2


if __name__ == '__main__':
    # input_str = raw_input()
    input_str = "1234 234 2"
    compute()
    print strcmp("432", "4321")

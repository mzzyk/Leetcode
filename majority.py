# -*- coding: utf-8 -*-
"""
Created on 2017/3/11

@author: zyk
"""


class Solution(object):
    def majorityElement(self, nums):
        """
        给定大小为 n 的数组，求数组中出现超过 n/3 次的元素
        1. 蒙泰卡罗算法
        采用随机样本预测数据集的分布情况。
        :param nums:
        :return:
        """

        if not nums:
            return []
        import random
        candidate = set(random.choice(nums) for i in range(10))
        return [n for n in candidate if nums.count(n) > len(nums) / 3]

    def majorityElement1(self, nums):
        """
        给定大小为 n 的数组，求数组中出现超过 n/3 次的元素
        摩尔投票法
        使用俩个element， element1 与 element2, 二者统计个数初始值为 count1 = 1
        count2 =1，依次往后找元素，如果遇到element1 则count1 加1，遇到element2 则
        count2 加 1， 否则都减 1， 如果count为 0 则换element。最后，count大于0 的
         元素就是所求的值。
        :type nums: List[int]
        :rtype: List[int]
        """
        if len(nums) < 2:
            return nums
        ele1, ele2, c1, c2 = nums[0], -1, 1, 0
        for i in range(1, len(nums)):
            print nums[i], ele1, c1, ele2, c2
            if nums[i] not in [ele1, ele2]:
                # print '....'
                if c1 == 0:
                    ele1 = nums[i]
                elif c2 == 0:
                    ele2 = nums[i]
            if ele1 == nums[i]:
                c1 += 1
            elif ele2 == nums[i]:
                c2 += 1
            else:
                c1 -= 1
                c2 -= 1
        print ele2, ele1
        return [n for n in [ele1, ele2] if nums.count(n) > len(nums) / 3]


s = Solution()
nums = [6, 6, 6, 6, 7, 8, 9, 1, 2, 3, 4]
# print nums.count(233), nums.count(2333)
print s.majorityElement1(nums)

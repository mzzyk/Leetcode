# -*- coding: utf-8 -*-
"""
Created on 2017/3/10

@author: zyk
"""
import sys


def get_count(s1, s2):
    min_len = len(s1) if len(s1) < len(s2) else len(s2)
    num = 1
    k = 1
    while min_len > k and s1[k] == s2[k]:
        num += 1
        k += 1
    return num


def get_common_len(s1, s2):
    """
    get the max length of common substring
    :param s1:
    :param s2:
    :return:
    """
    if len(s1) > len(s2):
        s = s1
        s1 = s2
        s2 = s
    if len(s1) < 1:
        return 0
    max_len = 0
    n = len(s1)
    for i in range(n):
        if max_len > len(s1) - i - 1:
            break
        for j in range(len(s2)):
            if s1[i] == s2[j]:
                count = get_count(s1[i:], s2[j:])
                if count > max_len:
                    max_len = count
    return max_len


if __name__ == '__main__':
    try:
        while True:
            line1 = sys.stdin.readline().strip()
            line2 = sys.stdin.readline().strip()
            res = get_common_len(line1, line2)
            print 'res', res
            break
    except:
        pass

# -*- coding: utf-8 -*-
"""
Created on 2017/3/11

@author: zyk
"""


class Solution:
    # @param {integer[]} nums
    # @return {string}
    def largestNumber(self, nums):
        """
        Given a list of non negative integers, arrange them such that they form the largest number.
        For example, given [3, 30, 34, 5, 9], the largest formed number is 9534330.
        :param nums:
        :return:
        """
        if not nums:
            return 0
        numstr = [str(n) for n in nums]
        numstr.sort(cmp=lambda x, y: cmp(y + x, x + y))
        return ''.join(numstr).lstrip('0') or '0'

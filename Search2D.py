# -*- coding: utf-8 -*-
"""
Created on 2017/3/21

@author: zyk
"""


class Solution(object):
    def searchMatrix(self, matrix, target):
        """
        从右下角开始扫描:
        如果 d > target, 行减一
        如果 d < target, 列加一
        如果 d == target, 返回结果
        结束条件：行 i >=0 且 列 <列个数
        :type matrix: List[List[int]]
        :type target: int
        :rtype: bool
        """
        if not matrix:
            return False
        i, j = len(matrix) - 1, 0
        while i >= 0 and j < len(matrix[0]):
            if matrix[i][j] > target:
                i -= 1
            elif matrix[i][j] < target:
                j += 1
            else:
                return True
        return False


d = [
    [1, 3, 5, 7],
    [10, 11, 16, 20],
    [23, 30, 34, 50]
]
a = Solution()
print a.searchMatrix(d, 51)

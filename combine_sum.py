# -*- coding: utf-8 -*-
"""
Created on 2017/3/11

@author: zyk
"""


class Solution2(object):
    def decodeString(self, s):
        """
        :type s: str
        :rtype: str
        """
        if len(s) == 0:
            return ""
        import re
        m = ''
        while '[' in s:
            s = re.sub(r'(\d+)\[([a-z]*)\]', lambda m: int(m.group(1)) * m.group(2), s)
        return s


class Solution1(object):
    def trailingZeroes(self, n):
        """
        找出 n! 的末尾有多少个 0。
        n! 肯定是一个合数，那么就可以分解为多个质数相乘，而质数中只有 2 和 5 相乘结尾为0，
        而分解的结果中也一定是2 的个数大于 5 的个数，如：10*9*7*6*5*4*3*2*1，只有10和5才能分解出5，
        因此 5 的个数也就是末尾0的个数
        :type n: int
        :rtype: int
        """
        num = n / 5
        if n / 5 == 0:
            return 0
        return num + self.trailingZeroes(n / 5)


class Solution(object):
    nums = []
    res = []

    def combinationSum2(self, candidates, target):
        """
        动态规划算法：
        数组为 arr, 首先对arr 进行排序
        那么 dp[n] = arr[i] + dp[n - arr[i]], 其中 i 为 arr中　＜＝　ｎ　全部索引。
        当选定　i 之后，那么 dp[n-arr[i]] = n-arr[i] - arr[j] + dp[n-arr[i]-arr[j]] >=0 ，
        其中 j > i 且 arr <=n-arr[i] - arr[j] (由于数据已经排序，故可排除重复的情况)
        :type candidates: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        if len(candidates) < 1 or min(candidates) > target:
            return []
        self.nums = candidates
        alr = []
        remain = []
        candidates.sort()
        for i in range(len(candidates)):
            if target >= candidates[i]:
                remain.append(i)
        self.res = []
        self.combination(target, alr, remain)
        result = []
        for x in self.res:
            x.sort()
            if x not in result:
                result.append(x)
        print len(result), len(self.res)
        return result

    def combination(self, sum, already, remain):
        import copy
        if not remain:
            return
        for i in remain:
            if i > 0 and self.nums[i] == self.nums[i-1]:
                continue
            alr = copy.copy(already)
            alr.append(i)
            s1 = sum - self.nums[i]
            if s1 == 0:
                self.res.append([self.nums[x] for x in alr])
            rem = []
            for j in remain:
                if (j > max(alr)) and s1 >= self.nums[j]:
                    rem.append(j)
            self.combination(s1, alr, rem)


a = Solution()
d = [23, 32, 22, 19, 29, 15, 11, 26, 28, 20, 34, 5, 34, 7, 28, 33, 30, 30, 16, 33, 8, 15, 28, 26, 17, 10, 25, 12, 6, 17,
     30, 16, 6, 10, 23, 22, 20, 29, 14, 5, 6, 5, 5, 6, 29, 20, 34, 24, 16, 7, 22, 11, 17, 7, 33, 21, 13, 15, 29, 6, 19,
     16, 10, 21, 21, 28, 8, 6]

target = 27
d = [10, 1, 2, 7, 6, 1, 5]
target = 8
# d= [1]
# target = 1

print a.combinationSum2(d, target)

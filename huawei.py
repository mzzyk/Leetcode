# -*- coding: utf-8 -*-
"""
Created on 2017/3/24

@author: zyk
"""

import numpy as np


def decprim(num):
    """
    质数分解：将一个合数分解为几个质数相乘
    :param num:
    :return:
    """
    k = 2
    res = []
    while num > 1:
        if num % k == 0:
            res.append(k)
            num /= k
            k = 2
        else:
            k += 1
    return res


def gcd(m, n):
    """
    求最大公约数
    :param m:
    :param n:
    :return:
    """
    if n == 0:
        return m
    return gcd(n, m % n)


# def test1():
#     seq = raw_input()
#     seq = seq.strip()
#     for x in list(seq):
#         if x == 'R':
#             tmp = init[-2:]
#             init[-2:] = init[:2]
#             init[:2] = [tmp[1], tmp[0]]
#         elif x == 'L':
#             tmp = init[:2]
#             init[:2] = init[-2:]
#             init[-2:] = [tmp[1], tmp[0]]
#         elif x == 'F':
#             a, b = init[2], init[3]
#             init[2:4] = init[-2:]
#             init[-2:] = [b, a]
#         elif x == 'B':
#             a, b = init[4], init[5]
#             init[-2:] = init[2:4]
#             init[2:4] = [b, a]
#         elif x == 'A':
#             a, b = init[2], init[3]
#             init[2:4] = init[:2]
#             init[:2] = [b, a]
#         elif x == 'C':
#             a, b = init[:2]
#             init[:2] = init[2:4]
#             init[2:4] = [b, a]
#     print ''.join(init)

def compute_path(x, path):
    """
    搜索最短路径，实质是图的搜索，可以使用深度遍历
    递归思想:
    如果通往目的路径为path，那么当前的点是否在path中，只需要考虑它的子节点是否也在path中
    一直递归，直到叶子节点，如果叶子节点是target，将该路径加入结果，否则，返回父节点从其它分支遍历。
    递归函数的终止条件：
    1 要加入的点已经在path中
    2 要加入的点就是target
    3 当前的点不符合任何扩展条件，即不可达，有大雾, 那么 for 循环不会执行，自动结束该分支的递归。

    :param x:
    :param path:
    :return:
    """
    if x in path:
        return
    else:
        path.append(x)
    if x == X - 1:
        res.append(path)
        return
    for i in range(len(m[0])):
        if 0 < m[x][i] < 1000 and i != Y - 1:
            p1 = copy.copy(path)
            compute_path(i, p1)


import sys
import copy

if __name__ == '__main__':
    m = [[0, 2, 10, 5, 3, 1000],
         [1000, 0, 12, 1000, 1000, 10],
         [1000, 1000, 0, 1000, 7, 1000],
         [2, 1000, 1000, 0, 2, 1000],
         [4, 1000, 1000, 1, 0, 1000],
         [3, 1000, 1, 1000, 2, 0]]
    try:
        while True:
            X = int(raw_input())
            Y = int(raw_input())
            res = []
            compute_path(4, [])
            min_path = 1000
            index = -1
            for i in range(len(res)):
                if sum(res[i]) < min_path:
                    index = i
            ress = res[index]
            if len(ress) == 0:
                print 1000
                print []
                break
            hours = 0
            for i in range(1, len(ress)):
                hours += m[ress[i - 1]][ress[i]]
            print hours
            print [x + 1 for x in res[index]]

    except:
        pass

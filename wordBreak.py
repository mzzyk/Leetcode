# -*- coding: utf-8 -*-
"""
Created on 2017/3/10

@author: zyk
"""


class Solution(object):
    def wordBreak(self, s, wordDict):
        """
        就是要给定一个字符串和一个字典，判断该字符串中等词是否由字典中的词组成。
        s = "leetcode", dict = ["leet", "code"]， return True
        结题思路：
        i = 1->n, 依次求前 i 个字符组成的子串是否在字典中(可以是几个字典中的词组成的)
        如果是，那么将结果存进(key=i,value=True)的字典 indict 中
        否则，i += 1，继续上述步骤。
        最后，检查key=n时是否为 True
        在考查前 i 个字符组成的子串在字典中时，有俩种情况：
        1. 子串整体在字典中
        2. 遍历结果字典 indict 中全部 key 值，检查 key到 i 部分的字符串是否在字典中，
        如果是，那么也将该 i 值存入结果
        :type s: str
        :type wordDict: List[str]
        :rtype: bool
        """
        if len(s) < 1:
            return True
        indict = {}
        for i in range(1, len(s) + 1):
            if s[:i] in wordDict:
                indict[i] = True
            else:
                sub_s = s[:i]
                for k in indict.keys():
                    if sub_s[k:] in wordDict:
                        indict[i] = True
                        break
        return indict.get(len(s), False)


a = Solution()
s = "s"
dict = []
# s= "cars"
# dict = ['car', 'ca', 'rs']
print a.wordBreak(s, dict)

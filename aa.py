# -*- coding: utf-8 -*-
"""
Created on 2017/9/10

@author: zyk
"""


class Solution:
    def __init__(self):
        self.stack1 = []
        self.stack2 = []

    def push(self, node):
        # write code here
        self.stack1.append(node)

    def pop(self):
        l = len(self.stack1)
        for i in range(l-1):
            self.stack2.append(self.stack1.pop())
        res = self.stack1.pop()
        for i in range(l - 1):
            self.stack1.append(self.stack2.pop())
        return res


s = Solution()
print s.NumberOf1(-3)

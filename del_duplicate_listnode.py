# -*- coding: utf-8 -*-
"""
Created on 2017/3/10

@author: zyk
"""


# Definition for singly-linked list.
class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution(object):
    def deleteDuplicates(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        if not head:
            return None
        curr = head
        while curr:
            t = curr.next
            while t and curr.val == t.val:
                t = t.next
            if t:
                curr.next = t
            else:
                curr.next = None
            curr = curr.next
        return head


def printnode(head):
    while head:
        print head.val
        head = head.next


a = Solution()
d1 = ListNode(1)
d2 = ListNode(2)
d3 = ListNode(2)
d1.next = d2
d2.next = d3
# printnode(d1)
r = a.deleteDuplicates(d3)
printnode (r)

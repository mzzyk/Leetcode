# -*- coding: utf-8 -*-
"""
Created on 2017/3/16

@author: zyk
"""


class Solution1(object):
    nums = []
    res = []

    def combinationSum2(self, candidates, target):
        """
        动态规划算法：
        数组为 arr, 首先对arr 进行排序
        那么 dp[n] = arr[i] + dp[n - arr[i]], 其中 i 为 arr中　＜＝　ｎ　全部索引。
        当选定　i 之后，那么 dp[n-arr[i]] = n-arr[i] - arr[j] + dp[n-arr[i]-arr[j]] >=0 ，
        其中 j > i 且 arr <=n-arr[i] - arr[j] (由于数据已经排序，故可排除重复的情况)
        :type candidates: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        if len(candidates) < 1 or min(candidates) > target:
            return []
        self.nums = candidates
        alr = []
        remain = []
        candidates.sort()
        for i in range(len(candidates)):
            if target >= candidates[i]:
                remain.append(i)
        self.res = []
        self.combination(target, alr, remain)
        result = []
        for x in self.res:
            x.sort()
            if x not in result:
                result.append(x)
        return result

    def combination(self, sum, already, remain):
        import copy
        if not remain:
            return
        for i in remain:
            alr = copy.copy(already)
            alr.append(i)
            s1 = sum - self.nums[i]
            if s1 == 0:
                self.res.append([self.nums[x] for x in alr])
            rem = []
            for j in remain:
                if (j > max(alr)) and s1 >= self.nums[j]:
                    rem.append(j)
            self.combination(s1, alr, rem)


class Solution(object):
    def combinationSum(self, candidates, target):
        """
        Given candidate set a = [2, 3, 6, 7] and target 7, Output: [[7],[2, 2, 3]]
        先对数组进行排序，这样考虑 a[0]与其他元素的全部组合之后，就可以不在考虑 a[0], 从a[1]开始继续。
        DP: dp[7] = a[i] + dp[7-a[i]], 其中 a[i] =< 7-a[i]
        :type candidates: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        if not candidates or min(candidates) > target:
            return []
        res = []
        candidates.sort()
        self.comb2(candidates, target, [], res)
        return res

    def comb2(self, nums, sum, path, res):
        import copy
        num = []
        for x in nums:
            if x <= sum:
                if not path:
                    num.append(x)
                elif path[-1] <= x:
                    num.append(x)
        if not num:
            return
        for x in num:
            p = copy.copy(path)
            p.append(x)
            sum1 = sum - x
            if sum1 == 0:
                res.append(p)
                return
            else:
                self.comb2(nums, sum1, p, res)


a = Solution()
d = [8, 7, 4, 3]
target = 11
print a.combinationSum(d, target)

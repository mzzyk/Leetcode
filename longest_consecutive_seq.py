# -*- coding: utf-8 -*-
"""
Created on 2017/3/11

@author: zyk
"""


class Solution(object):
    def longestConsecutive(self, nums):
        """
        以空间换时间，将给定数组生成哈希列表，那么就可以快速访问某个数是否存在
        遍历哈希列表，对当前值：先从依次递增 1 直到无合适值，然后依次递减 1 直到无合适值
        统计前后共有多少个数。
        遍历结束之后，记录下最大的统计数，即为所求答案。
        :type nums: List[int]
        :rtype: int
        """
        nums_dict = {}
        for n in nums:
            nums_dict[n] = 1
        max_count = 0
        for k in nums_dict.keys():
            if nums_dict.get(k, -1) == -1:
                continue
            count = 1
            k1 = k2 = k
            while nums_dict.get(k1 + 1, -1) != -1:
                # print 'asdf'
                count += 1
                k1 += 1
                del (nums_dict[k1])
            while nums_dict.get(k2 - 1, -1) != -1:
                count += 1
                k2 -= 1
                del (nums_dict[k2])
            del (nums_dict[k])
            if count > max_count:
                max_count = count
        return max_count


a = Solution()
nums = [100, 4, 200, 1, 3, 2, 101, 100, 99, 98, 102, 103]
print a.longestConsecutive(nums)

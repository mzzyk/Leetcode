# -*- coding: utf-8 -*-
"""
Created on 2017/3/15

@author: zyk
"""


class Solution1(object):
    def calculate(self, s):
        """
        求一个简单的数学运算表达式的值， 如"3+2*2" = 7
        将字符串按照运算符号进行分隔，得到数字字符与运算符号俩个数组 nums 和 ops，
        依据运算的优先级，
        1. 首先计算乘除法，那么在 ops中遍历‘＊’　和　‘／’　 并将对应的数字进行运算，由于运算数字比符号多一个，
        因此每个运算符号在 ops 中的索引 i， 在 nums 中对应 i 与 i+1 俩个数,  然后对折俩数进行计算，结果存入 nums的 i 中，删除 i+1
        2. 剩余部分就是加减法依次计算即可。
        :type s: str
        :rtype: int
        """
        s = s.strip()
        if s.isdigit():
            return int(s)
        nums = ['', ]
        ops = []
        for x in s:
            if x in ['+', '-', '*', '/']:
                ops.append(x)
                nums.append('')
            else:
                nums[-1] += x
        i = 0
        while i < len(ops):
            if ops[i] == '/':
                nums[i] = int(nums[i]) / int(nums[i + 1])
                del (nums[i + 1])
                del (ops[i])
            elif ops[i] == '*':
                nums[i] = int(nums[i]) * int(nums[i + 1])
                del (nums[i + 1])
                del (ops[i])
            else:
                i += 1
        # print nums, ops
        res = int(nums[0])
        for i in range(1, len(nums)):
            if ops[i - 1] == '+':
                res += int(nums[i])
            else:
                res -= int(nums[i])
        return res


class Solution(object):
    def calculate(self, s):
        """
        计算 "(1+(4+5+2)-3)+(6+8)" = 23，只有加减和括号
        将 str 转换成数字和运算符数组， ss
        依次遍历数组，数字和运算符合入栈，一旦遇到右括号，一直出栈并进行运算，直到遇到左括号
        将左括号出栈，并将计算结果入栈，然后继续遍历

        :type s: str
        :rtype: int
        """
        s = s.replace(' ', '')
        if s.isdigit():
            return int(s)
        if '+' not in s and '-' not in s:
            import re
            pattern = re.search(r'\d+', s)
            if pattern:
                return int(pattern.group())
            else:
                return 0
        ss = []
        for x in s:
            if x in ['(', ')', '+', '-']:
                ss.append(x)
            else:
                if not ss:
                    ss.append(x)
                elif ss[-1].isdigit():
                    ss[-1] += x
                else:
                    ss.append(x)
        nums = []
        for i in range(0, len(ss)):

            if ss[i] == ')':
                tmp = []
                while True:
                    l = nums.pop()
                    if l == "(":
                        break
                    else:
                        tmp.append(l)
                tmp = tmp[::-1]
                sum = int(tmp[0])
                for x in range(1, len(tmp), 2):
                    if tmp[x] == '+':
                        sum += int(tmp[x + 1])
                    else:
                        sum -= int(tmp[x + 1])
                # print nums, tmp
                nums.append(sum)
            else:
                nums.append(ss[i])

        if len(nums) < 2:
            return nums[0]
        else:
            res = int(nums[0])
            for x in range(1, len(nums), 2):
                if nums[x] == '+':
                    res += int(nums[x + 1])
                else:
                    res -= int(nums[x + 1])
            return res

s = "(1+(4+5+2)-3)+(6+8)"
s = " 1 + 1 "
a = Solution()
print a.calculate(s)
